Boss’s Day – October 16
This day is founded by Patricia Bays Haroski in 1958 for her father and she registered this day on her fathers’ birthday which is October 16. This day is celebrated by employees for their bosses because of their kindness and their fairness to thanks them. If you are working in a company and you believe your bosses’ fairness and kindness, you might organize a simple party in your organization to thank for your boss.

October Image Source: https://www.123calendars.com/images/2019/October/October-2019-Calendar.jpg

Columbus Day – October 14
This day is the celebration about arrival of the Christopher Columbus’s in the Americas on October 12, 1492 but in United States of America it is celebrated on October 14 on 2019. It is celebrated by people across the America continent by every country (United States of America, Canada, Mexico, Argentina, Brazil, Paraguay, Uruguay, Peru etc.). Christopher Columbus was an Italian Explorer and his aim was to explore new areas across the world. He set sail from the Europe and traveled many days to find continent America or the new world as he calls. The exploration took approximately three months.

[Columbus Day](https://en.wikipedia.org/wiki/Columbus_Day) has very different names on South America. Some Latin American countries call this day as Dia de la Raza which means “Day of the Race”. Many of the American countries from both South and North America has a federal holiday on this day.

On your printable calendar, you can easily add this day by clicking the Add Holidays button. On this day you are definitely going to see celebrations on streets and in city center. If you are a business person or a person who drives in the city, you should not forget the traffic jam because of the celebrations. Because citizens would like to celebrate this day with drinking beverages, drinks and alcohol.

Source: https://www.123calendars.com/october-calendar.html